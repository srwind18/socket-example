export const organizationHandlers = (io, socket, notebook, users) => {
	socket.on("join", (query) => {
		const { room } = query;
		socket.join(room);
		users[socket.id] = { room };
		if (!notebook[query.room]) {
			notebook[query.room] = new Set();
		}

		io.in(query.room).emit("notebooks-list", Array.from(notebook[query.room]));
	});

	socket.on("leave", (query) => {
		io.in(query.room).emit("user-leave", query.value);

		socket.leave(query.room);
		delete users[query.userID];
	});

	socket.on("addNotebook", (query) => {
    console.log(query);
		if (notebook[query.room]) {
			notebook[query.room].add(query.notebook);
			io.in(query.room).emit("add-notebook", query.notebook);
			io.in(query.room).emit("notebooks-list", Array.from(notebook[query.room]));
		} else {
			notebook[query.room] = new Set();
			notebook[query.room].add(query.notebook);
			io.in(query.room).emit("add-notebook", query.notebook);
			io.in(query.room).emit("notebooks-list", Array.from(notebook[query.room]));
		}
	});
	socket.on("removeNotebook", (query) => {
		notebook[query.room].delete(query.notebook);
		io.in(query.room).emit("notebooks-list", Array.from(notebook[query.room]));
	});
};
