export const notebooksHandlers = (io, socket, users) => {
	socket.on("join", (query) => {
		const { notebook } = query;
		if (notebook) {
			users[socket.id] = { ...users[socket.id], notebook };
			socket.join(notebook);
			const usersList = io.in(notebook).allSockets();
			usersList.then((d) => {
				io.in(notebook).emit("users-working", Array.from(d));
			});
		}
	});
	socket.on("leave", (query) => {
		const { notebook } = query;
		if (notebook) {
			if (users[socket.id] && users[socket.id].notebook) {
				delete users[socket.id].notebook;
			}
			socket.leave(notebook);

			const usersList = io.in(notebook).allSockets();
			usersList.then((d) => {
				io.in(notebook).emit("users-working", Array.from(d));
			});
		}
	});
	socket.on("usersWorking", (query) => {
		const usersList = io.in(query.notebook).allSockets();

		usersList.then((d) => {
			io.to(socket.id).emit("users-working", Array.from(d));
		});
	});
	socket.on("updateText", (query) => {
		io.in(query.notebook).emit("update-text", query.value);
	});
};
