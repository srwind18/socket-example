import express from "express";
import { createServer } from "http";
import { Server } from "socket.io";
import { notebooksHandlers } from "./controller/notebooks.js";
import { organizationHandlers } from "./controller/organizations.js";

const app = express();
const httpServer = createServer(app);
const log = console.log;
const io = new Server(httpServer, {
	cors: {
		origin: "*",
	},
});
const users = {};
const notebook = {};

app.get("/", (req, res) => {
	res.status(200).send({ message: "Socket.IO example api" });
});

io.on("connection", (socket) => {
	const token = socket.handshake.auth.token;
	log(token);
});

// Organizations
const organizationNamespace = io.of("/organization");

organizationNamespace.on("connection", (socket) => {
	const rooms = socket.rooms;
	log("Organization");
	log(rooms);
	organizationHandlers(organizationNamespace, socket, notebook, users);
});

// Notebooks
const notebookNamespace = io.of("/notebooks");

notebookNamespace.on("connection", (socket) => {
	const rooms = socket.rooms;
	log("Notebooks");
	log(rooms);
	notebooksHandlers(notebookNamespace, socket, users);
});

httpServer.listen(5050);
