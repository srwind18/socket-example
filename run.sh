#!/bin/bash

cd ./client && yarn install
cd ../server && yarn install
cd .. && yarn start
