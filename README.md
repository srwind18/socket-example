## easy start with docker
`docker-compose up -d`

## Dev local start
in root directory:
`yarn start:dev`

### OR

Frontend
`cd ./client && yarn && yarn start`

Backend
`cd ./server && yarn && yarn start`
