import { useCallback, useEffect, useRef, useState } from "react";
import { io } from "socket.io-client";

const useNotebooks = (notebook) => {
	const [users, setUsers] = useState([]);
	const [text, setText] = useState("");
	const [currentID, setCurrentID] = useState(null);

	const socketRef = useRef(null);

	const joinNotebook = useCallback((nb) => {
		socketRef.current.emit("join", { notebook: nb });
	}, []);
	const leaveNotebook = useCallback((nb) => socketRef.current.emit("leave", { notebook: nb }), []);

	useEffect(() => {
		socketRef.current = io("http://localhost:5050/notebooks", {
			transport: ["websocket"],
			auth: { token: "sampletoken" },
		});

		socketRef.current.on("users-working", (users) => {
			console.log(users);
			setUsers(users);
		});
		socketRef.current.on("update-text", (data) => {
			setText(data);
		});
		if (notebook) {
			joinNotebook(notebook);
		}
		return () => {
			leaveNotebook(notebook);
			socketRef.current.disconnect();
		};
	}, [notebook]);

	const usersWorking = useCallback((notebook) => socketRef.current.emit("usersWorking", { notebook }), []);

	const updateNotebook = useCallback((notebook, text) => {
		socketRef.current.emit("updateText", { notebook, value: text });
	}, []);

	useEffect(() => {
		if (socketRef?.current.id) setCurrentID(socketRef.current.id);
	}, [socketRef?.current?.id]);

	useEffect(() => {
		setText("");
	}, [notebook]);

	return {
		text,
		currentID,
		notebookSocket: socketRef.current,
		users,
		usersWorking,
		updateNotebook,
		setText,
	};
};

export default useNotebooks;
