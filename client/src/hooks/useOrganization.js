import { useCallback, useContext, useEffect, useRef } from "react";
import { io } from "socket.io-client";
import { SocketContext } from "../App";

const useOrganization = (organization) => {
	const { setNotebooks, notebook, setNotebook, notebooks } = useContext(SocketContext);
	const socketRef = useRef(null);
	const handleAdding = useCallback(
		(data) => {
			setNotebooks([...notebooks, data]);
		},
		[notebooks]
	);
	useEffect(() => {
		setNotebook(null);
		socketRef.current = io("http://localhost:5050/organization", {
			transport: ["websocket"],
			auth: { token: "sampletoken" },
		});
		socketRef.current.on("add-notebook", handleAdding);
		socketRef.current.on("notebooks-list", (data) => setNotebooks(data));
		if (organization) {
			socketRef.current.emit("join", { room: organization });
		}
		return () => {
			socketRef.current.emit("leave", { room: organization });
			socketRef.current.disconnect();
		};
	}, [organization]);

	const addNotebook = useCallback((org, nb) => {
		console.log(org, nb);
		socketRef.current.emit("addNotebook", { room: org, notebook: nb });
	}, []);

	const removeNotebook = useCallback(
		(nbk) => {
			if (notebook === nbk) setNotebook(null);
			socketRef.current.emit("removeNotebook", { room: organization, notebook: nbk });
		},
		[organization, notebook]
	);

	return { addNotebook, removeNotebook };
};

export default useOrganization;
