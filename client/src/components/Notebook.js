import React, { useContext, useEffect, useState } from "react";
import { SocketContext } from "../App";
import useNotebooks from "../hooks/useNotebooks";
import styles from "./Notebook.module.scss";

const Notebook = () => {
	const { notebook } = useContext(SocketContext);
	const { currentID, text, updateNotebook, users, usersWorking, setText } = useNotebooks(notebook);

	const handleChange = (value) => {
		updateNotebook(notebook, value);
	};

	useEffect(() => {
		setText("");
		usersWorking(notebook);
	}, [notebook]);

	return (
		<div className={styles.textContainer}>
			<div className={styles.name}>
				Notebook: <span>{notebook}</span>
			</div>
			<div>
				Users: <span>{users.map((el) => (el === currentID ? "You" : el)).join(", ")}</span>
			</div>
			<div>
				<textarea
					rows={10}
					className={styles.textarea}
					value={text}
					onChange={({ target }) => handleChange(target.value)}
				/>
			</div>
		</div>
	);
};

export default Notebook;
