import { useContext } from "react";
import cls from "classnames";
import styles from "./Sidebar.module.scss";
import { SocketContext } from "../App";
const organizations = [
	{ id: "sample_org_12312", name: "GeneralOrg" },
	{ id: "sample_org_1231241234", name: "OtherOrg" },
];
const Sidebar = () => {
	const { organization, setOrganiztaion } = useContext(SocketContext);

	const handleConnection = (orgID) => {
		setOrganiztaion(orgID);
	};
	return (
		<div className={styles.container}>
			<h4>Organizations:</h4>
			<div className={styles.list}>
				{organizations.map((el) => (
					<button
						key={el.id}
						className={cls({ [styles.active]: organization === el.id })}
						onClick={() => handleConnection(el.id)}
					>
						{el.name}
					</button>
				))}
			</div>
		</div>
	);
};

export default Sidebar;
