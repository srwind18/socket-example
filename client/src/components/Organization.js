import React, { useContext, useEffect } from "react";
import rw from "random-words";
import cls from "classnames";
import { SocketContext } from "../App";
import useOrganization from "../hooks/useOrganization";
import Notebook from "./Notebook";
import styles from "./Organization.module.scss";
import { organizations } from "../constants";

const Organization = () => {
	const { organization, notebook, notebooks, setNotebook } = useContext(SocketContext);
	const { addNotebook, removeNotebook } = useOrganization(organization);

	useEffect(() => {
		setNotebook(null);
		return () => {};
	}, [organization]);

	return (
		<div className={styles.container}>
			<h3>{organizations.find((el) => el.id === organization)?.name}</h3>
			{organization && (
				<>
					<div>
						<button
							className={styles.addButton}
							onClick={() => addNotebook(organization, rw({ exactly: 2, join: "" }))}
						>
							Add Notebook +
						</button>
					</div>
					<div className={styles.tabline}>
						{notebooks &&
							notebooks.map((el) => (
								<div key={el} className={styles.tab}>
									<button
										className={cls(styles.tabButton, { [styles.active]: el === notebook })}
										onClick={() => setNotebook(el)}
									>
										{el}
									</button>
									<button className={styles.deleteButton} onClick={() => removeNotebook(el)}>
										x
									</button>
								</div>
							))}
					</div>
					<div>{notebook && <Notebook />}</div>
				</>
			)}
			{!organization && "Select organization"}
		</div>
	);
};

export default Organization;
