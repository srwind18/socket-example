import { createContext, useState } from "react";
import styles from "./App.module.scss";
import { Sidebar, Organization } from "./components";

export const SocketContext = createContext();

const SocketProvider = ({ children }) => {
	const [notebook, setNotebook] = useState(null);
	const [notebooks, setNotebooks] = useState([]);
	const [organization, setOrganiztaion] = useState(null);

	return (
		<SocketContext.Provider
			value={{
				organization,
				setOrganiztaion,
				notebook,
				setNotebook,
				notebooks,
				setNotebooks,
			}}
		>
			{children}
		</SocketContext.Provider>
	);
};

function App() {
	return (
		<SocketProvider>
			<div className={styles.wrapper}>
				<div className={styles.container}>
					<div className={styles.sidebar}>
						<Sidebar />
					</div>
					<div className={styles.content}>
						<Organization />
					</div>
				</div>
			</div>
		</SocketProvider>
	);
}

export default App;
